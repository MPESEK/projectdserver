const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const Weapons = require('../models/weapons');
var authenticate = require('../authenticate');
const weaponRouter = express.Router();

weaponRouter.use(bodyParser.json());
const cors = require('./cors');

weaponRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, (req,res,next) => {
    Weapons.find(req.query)
    .then((weapons) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(weapons);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, (req, res, next) => {
    Weapons.create(req.body)
    .then((weapon) => {
        console.log('Weapon Created ', weapon);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(weapon);
    }, (err) => next(err))
    .catch((err) => next(err));
})

module.exports = weaponRouter;
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const Mercenarys = require('../models/mercenarys');
var authenticate = require('../authenticate');
const mercenaryRouter = express.Router();
const Equipped = require('../models/equipped');
mercenaryRouter.use(bodyParser.json());
const cors = require('./cors');

mercenaryRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, (req,res,next) => {
    Mercenarys.find(req.query)
    .then((mercenarys) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(mercenarys);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, (req, res, next) => {
    Mercenarys.create(req.body)
    .then((mercenary) => {
        console.log('Weapon Created ', mercenary);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(mercenary);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    Mercenarys.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));    
});
mercenaryRouter.route('/:mercenaryId')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) => {
    console.log("hello dfeleting stuff")
    Mercenarys.findOneAndRemove({_id: req.params.mercenaryId})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));   
    
  
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Equipped.findOne({user: req.user._id})
    .then((equipped) => {
        console.log("equipped are", equipped)
        if(!equipped){
                Mercenarys.find({ _id: req.params.mercenaryId }).exec(function (err, mercenary){    
                // User result only available inside of this function!
                console.log("mercenary is", mercenary)
                console.log("weamercenary is", mercenary[0])
                Equipped.create({user: req.user._id,mercenarys: [mercenary[0]]})
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('weapons')
                    .populate('mercenarys')
                    .then((equipped) =>{
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                    
                })            
            })
            
        }
        else {
            if (equipped.mercenarys.indexOf(req.params.mercenaryId) < 10) {    
                console.log("XCVDVDFSV")
                equipped.mercenarys.push({ "_id": req.params.mercenaryId });
                equipped.save()
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('weapons')
                    .populate('mercenarys')
                    .then((equipped) => {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                })
                .catch((err) => {
                    return next(err);
                })
            }
    
   
  } 
  
})   
})

module.exports = mercenaryRouter;
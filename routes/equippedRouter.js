const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var authenticate = require('../authenticate');
const Equipped = require('../models/equipped');
const Weapons = require('../models/weapons');
const cors = require('./cors');
const equippedRouter= express.Router();

equippedRouter.use(bodyParser.json());

equippedRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors,authenticate.verifyUser, (req, res, next) => {
    

    if (!req.headers.authorization) {
        return res.json({ error: 'You do not have authorization' });
        }
    Equipped.findOne({user: req.user._id})   
    .populate('user')
    .populate('bench')
    .populate('mercenarys')
    .populate('currentShop')
    .then((equipped) => {
        
        if (equipped != null) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json( equipped);
        }
        else {
            Equipped.create({user: req.user._id})
            .then((equipped) => {
                Equipped.findById(equipped._id)
                .populate('user')
                .then((equipped) =>{
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(equipped);
                })
                
            })        
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(authenticate.verifyUser, (req, res, next) => {
    Equipped.findOne({user: {_id: req.user._id}})
    .populate('user')
    .then((equipped) => {
        if(!equipped){
            
            Equipped.create({user: req.user._id})
            .then((equipped) => {
                Equipped.findById(equipped._id)
                .populate('user')
                .then((equipped) =>{
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(equipped);
                })
                
            })            
        }
        else {
            err = new Error('weapon ' + equipped + ' not found');
            err.status = 404;
            return next(err);
        }
              
    }, (err) => next(err));
        

        

   
})
.delete(authenticate.verifyUser, (req, res, next) => {
    console.log(req.user)
    Equipped.find({})
    .populate('user')
    .populate('weapons')
    .find({user: req.user._id})
    .remove()
    .then((equipped) => {
        console.log('equipped removed ', equipped);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(equipped);
    }, (err) => next(err))  
});
equippedRouter.route('/:weaponId')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors,authenticate.verifyUser, (req,res,next) => {
    Equipped.findOne({user: req.user._id})
    .populate('user')
    .populate('bench')
    .populate('mercenarys')
    .then((equipped) => {
        if (!equipped) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json({"exists": false, "equipped": equipped});
        }
        else {
            if (equipped.weapons.indexOf(req.params.weaponId) < 3) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": false, "equipped": equipped});
            }
            else {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": true, "equipped": equipped});
            }
        }

    }, (err) => next(err))
    .catch((err) => next(err))
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Equipped.findOne({user: req.user._id})
    .then((equipped) => {
        console.log("equipped are", equipped)
        if(!equipped){
                Weapons.find({ _id: req.params.weaponId }).exec(function (err, weapon){    
                // User result only available inside of this function!
                console.log("weapon is", weapon)
                console.log("weapon is", weapon[0])
                Equipped.create({user: req.user._id,weapons: [weapon[0]]})
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('weapons')
                    .then((equipped) =>{
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                    
                })            
            })
            
        }
        else {
            if (equipped.weapons.indexOf(req.params.weaponId) < 3) {    
                console.log("XCVDVDFSV")
                equipped.weapons.push({ "_id": req.params.weaponId });
                equipped.save()
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('weapons')
                    .then((equipped) => {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                })
                .catch((err) => {
                    return next(err);
                })
            }
    
   
  } 
  
})   
})

equippedRouter.route('/mercenary/:mercenaryId')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors,authenticate.verifyUser, (req,res,next) => {
    Equipped.findOne({user: req.user._id})
    .populate('user')
    .populate('bench')
    .populate('mercenarys')
    .populate('currentShop')
    .then((equipped) => {
        if (!equipped) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json({"exists": false, "equipped": equipped});
        }
        else {
            if (equipped.weapons.indexOf(req.params.weaponId) < 3) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": false, "equipped": equipped});
            }
            else {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": true, "equipped": equipped});
            }
        }

    }, (err) => next(err))
    .catch((err) => next(err))
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Equipped.findOne({user: req.user._id})
    .then((equipped) => {
        console.log("equipped are", equipped)
        if(!equipped){
                Mercenarys.find({ _id: req.params.mercenaryId }).exec(function (err, mercenary){    
                // User result only available inside of this function!
                console.log("weapon is", mercenary)
                console.log("weapon is", mercenary[0])
                Equipped.create({user: req.user._id,mercenarys: [mercenary[0]]})
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('mercenarys')
                    .populate('bench')
                    .populate('currentShop')
                    .then((equipped) =>{
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                    
                })            
            })
            
        }
        else {
            if (equipped.mercenarys.indexOf(req.params.mercenaryId) < 3) {    
                console.log("XCVDVDFSV")
                equipped.mercenarys.push({ "_id": req.params.mercenaryId });
                equipped.save()
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('bench')
                    .populate('mercenarys')
                    .populate('currentShop')
                    .then((equipped) => {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                })
                .catch((err) => {
                    return next(err);
                })
            }
    
   
  } 
}, (err) => next(err))
.catch((err) => next(err))
  
})
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) => {
    console.log("hello dfeleting stuff")
    Equipped.findOne({user: req.user._id})
    .populate('mercenarys')
    .populate('bench')
    .then((equipped) => {
        console.log("req is", req.params.mercenaryId)
        Equipped.findOneAndUpdate({user: req.user._id},{$pull: {mercenarys: req.params.mercenaryId}})
        
    
        .then((equipped) => {
            Equipped.findById(equipped._id)
            .populate('user')
            .populate('bench')
            .populate('mercenarys')
            .then((equipped) => {
                console.log('Favorite removed ', equipped);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(equipped);
            })
        }, (err) => next(err));    
        console.log('Favorite removed ', equipped);
       
    }, (err) => next(err));  
  
});

equippedRouter.route('/bench/:mercenaryId')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors,authenticate.verifyUser, (req,res,next) => {
    Equipped.findOne({user: req.user._id})
    .populate('user')
    .populate('bench')
    .populate('mercenarys')
    .populate('currentShop')
    .then((equipped) => {
        if (!equipped) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json({"exists": false, "equipped": equipped});
        }
        else {
            if (equipped.weapons.indexOf(req.params.weaponId) < 3) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": false, "equipped": equipped});
            }
            else {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": true, "equipped": equipped});
            }
        }

    }, (err) => next(err))
    .catch((err) => next(err))
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Equipped.findOne({user: req.user._id})
    .then((equipped) => {
        console.log("equipped are", equipped)
        if(!equipped){
                Mercenarys.find({ _id: req.params.mercenaryId }).exec(function (err, mercenary){    
                // User result only available inside of this function!
                console.log("weapon is", mercenary)
                console.log("weapon is", mercenary[0])
                Equipped.create({user: req.user._id,bench: [mercenary[0]]})
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('mercenarys')
                    .populate('bench')
                    .populate('currentShop')
                    .then((equipped) =>{
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                    
                })            
            })
            
        }
        else {
            if (equipped.bench.indexOf(req.params.mercenaryId) < 3) {    
                console.log("XCVDVDFSV")
                equipped.bench.push({ "_id": req.params.mercenaryId });
                equipped.save()
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('bench')
                    .populate('mercenarys')
                    .populate('currentShop')
                    .then((equipped) => {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                })
                .catch((err) => {
                    return next(err);
                })
            }
    
   
  }
})
  
})
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) => {
    console.log("hello dfeleting stuff")
    Equipped.findOne({user: req.user._id})
    .populate('mercenarys')
    .populate('bench')
    .populate('currentShop')
    .then((equipped) => {
        console.log("req is", req.params.mercenaryId)
        Equipped.findOneAndUpdate({user: req.user._id},{$pull: {bench: req.params.mercenaryId}})
        
    
        .then((equipped) => {
            Equipped.findById(equipped._id)
            .populate('bench')
            .populate('mercenarys')
            .populate('currentShop')
            .then((equipped) => {
                console.log('Favorite removed ', equipped);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(equipped);
            })
        }, (err) => next(err));    
        console.log('Favorite removed ', equipped);
       
    }, (err) => next(err));  
  
});

equippedRouter.route('/currentShop/:mercenaryId')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors,authenticate.verifyUser, (req,res,next) => {
    Equipped.findOne({user: req.user._id})
    .populate('user')
    .populate('bench')
    .populate('mercenarys')
    .populate('currentShop')
    .then((equipped) => {
        if (!equipped) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json({"exists": false, "equipped": equipped});
        }
        else {
            if (equipped.weapons.indexOf(req.params.weaponId) < 3) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": false, "equipped": equipped});
            }
            else {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": true, "equipped": equipped});
            }
        }

    }, (err) => next(err))
    .catch((err) => next(err))
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Equipped.findOne({user: req.user._id})
    .then((equipped) => {
        console.log("equipped is", equipped)
        if(!equipped){
                Mercenarys.find({ _id: req.params.mercenaryId }).exec(function (err, mercenary){    
                // User result only available inside of this function!
                console.log("weapon is", mercenary)
                console.log("weapon is", mercenary[0])
                Equipped.create({user: req.user._id,currentShop: [mercenary[0]]})
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('mercenarys')
                    .populate('bench')
                    .populate('currentShop')
                    .then((equipped) =>{
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                    
                })
                .catch((err) => {
                    return next(err);
                })
            })
            .catch((err) => next(err))
            
        }
        else {
            if (1) {    
                console.log("XCVDVDFSV")
                equipped.currentShop.push({ "_id": req.params.mercenaryId });
                equipped.save()
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('bench')
                    .populate('mercenarys')
                    .populate('currentShop')
                    .then((equipped) => {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                })
                .catch((err) => {
                    return next(err);
                })
            }
    
   
  }
})
  
})
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) => {
    console.log("hello dfeleting stuff")
    Equipped.findOne({user: req.user._id})
    .populate('mercenarys')
    .populate('bench')
    .populate('currentShop')
    .then((equipped) => {
        console.log("req is", req.params.mercenaryId)
        Equipped.findOneAndUpdate({user: req.user._id},{$pull: {currentShop: req.params.mercenaryId}})
        
    
        .then((equipped) => {
            Equipped.findById(equipped._id)
            .populate('bench')
            .populate('mercenarys')
            .populate('currentShop')
            .then((equipped) => {
                console.log('Favorite removed ', equipped);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(equipped);
            })
        }, (err) => next(err));    
        console.log('Favorite removed ', equipped);
       
    }, (err) => next(err));  
  
});

equippedRouter.route('/toggleQuest')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors,authenticate.verifyUser, (req,res,next) => {
    Equipped.findOne({user: req.user._id})
    .populate('user')
    .populate('bench')
    .populate('mercenarys')
    .then((equipped) => {
        if (!equipped) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json({"exists": false, "equipped": equipped});
        }
        else {
            if (equipped.weapons.indexOf(req.params.weaponId) < 3) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": false, "equipped": equipped});
            }
            else {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": true, "equipped": equipped});
            }
        }

    }, (err) => next(err))
    .catch((err) => next(err))
})
.put(cors.cors,authenticate.verifyUser, (req,res,next) => {
    Equipped.findOne({user: req.user._id})
    .populate('user')
    .populate('bench')
    .populate('mercenarys')
    .then((equipped) => {
        if (!equipped) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json({"exists": false, "equipped": equipped});
        }
        else {
            if (equipped.questCompleted) {
                console.log("quest completed")
                Equipped.findOneAndUpdate({user: req.user._id},{"questCompleted": false})
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('mercenarys')
                    .populate('bench')
                    .populate('currentShop')
                    .then((equipped) =>{
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                    
                })           
              
            }
            else {
                console.log("quest NOT completed")
                Equipped.findOneAndUpdate({user: req.user._id},{"questCompleted": true})
                .then((equipped) => {
                    Equipped.findById(equipped._id)
                    .populate('user')
                    .populate('mercenarys')
                    .populate('bench')
                    .populate('currentShop')
                    .then((equipped) =>{
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(equipped);
                    })
                    
                })       
              
            }
        }

    }, (err) => next(err))
    .catch((err) => next(err))
})


equippedRouter.route('/CS')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) => {
    console.log("hello dfeleting stuff")
    Equipped.findOne({user: req.user._id})
    .populate('mercenarys')
    .populate('bench')
    .populate('currentShop')
    .then((equipped) => {
        console.log("req is", req.params.mercenaryId)
        Equipped.findOneAndUpdate({user: req.user._id},{$set: { currentShop: []}})
        
    
        .then((equipped) => {
            Equipped.findById(equipped._id)
            .populate('bench')
            .populate('mercenarys')
            .populate('currentShop')
            .then((equipped) => {
                console.log('Favorite removed ', equipped);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(equipped);
            })
        }, (err) => next(err));    
        console.log('Favorite removed ', equipped);
       
    }, (err) => next(err));  
  
})
equippedRouter.route('/changeGold/:goldAmount')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors,authenticate.verifyUser, (req,res,next) => {
    Equipped.findOne({user: req.user._id})
    .populate('user')
    .populate('bench')
    .populate('mercenarys')
    .then((equipped) => {
        if (!equipped) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json({"exists": false, "equipped": equipped});
        }
        else {
            if (equipped.weapons.indexOf(req.params.weaponId) < 3) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": false, "equipped": equipped});
            }
            else {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": true, "equipped": equipped});
            }
        }

    }, (err) => next(err))
    .catch((err) => next(err))
})
.put(cors.cors,authenticate.verifyUser, (req,res,next) => {
    Equipped.findOne({user: req.user._id})
    .populate('user')
    .populate('bench')
    .populate('mercenarys')
    .then((equipped) => {
        if (!equipped) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json({"exists": false, "equipped": equipped});
        }
        else {
            console.log("request is", req)
            
            Equipped.findOneAndUpdate({user: req.user._id},{"gold": req.params.goldAmount})
            .then((equipped) => {
                Equipped.findById(equipped._id)
                .populate('user')
                .populate('mercenarys')
                .populate('bench')
                .populate('currentShop')
                .then((equipped) =>{
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(equipped);
                })
                
            })           
            
        }
            
        

    }, (err) => next(err))
    .catch((err) => next(err))
})
module.exports = equippedRouter;
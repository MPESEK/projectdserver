[
    {
        "hitPoints": 10,
        "attackSpeed": 10,
        "attackDamage": 10,
        "armorClass": 10,
        "magicResist": 10,
        "isLegendary": 0,
        "legendaryVersionId": "5e0127c303399e157c82946c",
        "name": "celestial_priest_ethereal",
        "image": "celestial_priest_ethereal.png",
        "origin": "celestial",
        "classOne": "priest",
        "classTwo": null
        
    },
    {
        "hitPoints": 10,
        "attackSpeed": 10,
        "attackDamage": 10,
        "armorClass": 10,
        "magicResist": 10,
        "isLegendary": 0,
        "legendaryVersionId": "5e0127c303399e157c82946d",
        "name": "celestial_ranged",
        "image": "celestial_ranged.png",
        "origin": "celestial",
        "classOne": "ranged",
        "classTwo": null
        
    },
    {
        "hitPoints": 10,
        "attackSpeed": 10,
        "attackDamage": 10,
        "armorClass": 10,
        "magicResist": 10,
        "isLegendary": 0,
        "legendaryVersionId": "5e0127c303399e157c82946e",
        "name": "celestial_warrior",
        "image": "celestial_warrior.png",
        "origin": "celestial",
        "classOne": "warrior",
        "classTwo": null
        
    },
    {
        "hitPoints": 10,
        "attackSpeed": 10,
        "attackDamage": 10,
        "armorClass": 10,
        "magicResist": 10,
        "isLegendary": 0,
        "legendaryVersionId": "5e0127c303399e157c82946f",
        "name": "celestial_warrior_ethereal",
        "image": "celestial_warrior_ethereal.png",
        "origin": "celestial",
        "classOne": "warrior",
        "classTwo": null
        
    }
]
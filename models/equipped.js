const mongoose = require('mongoose');
const Schema = mongoose.Schema;





var equippedSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    gold: { type:Number,default:120},
    questCompleted: { type:Boolean,default:true},
    currentShop: [{type: mongoose.Schema.Types.ObjectId, ref: 'Mercenary'}],
    mercenarys: [{type: mongoose.Schema.Types.ObjectId, ref: 'Mercenary'}],
    bench: [{type: mongoose.Schema.Types.ObjectId, ref: 'Mercenary'}],
    
    weapons: [{type: mongoose.Schema.Types.ObjectId, ref: 'Weapon'}],
}, {
    timestamps: true
});

var Equipped = mongoose.model('Equipped', equippedSchema);

module.exports = Equipped;
const mongoose = require('mongoose');
const Schema = mongoose.Schema;



var mercenarySchema = new Schema({
    name:
    {
        type: String,
        unique: false
    },
    image:
    {
        type: String,
        unique: false
    },
    origin: {
        type: String,
        unique: false
    },
    classOne: {
        type: String,
        unique: false
    },
    classTwo: {
        type: String,
        unique: false
    },
    hitPoints:   {
        type: Number,
        default: false
    },
    attackSpeed:   {
        type: Number,
        default: false
    },
    attackDamage:   {
        type: Number,
        default: false
    },
    armorClass:   {
        type: Number,
        default: false
    },
    magicResist:   {
        type: Number,
        default: false
    },
    isLegendary:   {
        type: Number,
        default: false
    },
    legendaryVersionId:   {
        type: String, 
    },
    
    
}, {
    timestamps: true
});

var Mercenarys = mongoose.model('Mercenary', mercenarySchema);

module.exports = Mercenarys;
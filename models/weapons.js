const mongoose = require('mongoose');
const Schema = mongoose.Schema;




var weaponSchema = new Schema({
    name:
    {
        type: String,
        unique: false
    },
    attackSpeed:   {
        type: Number,
        
    },
    attackDamage:   {
        type: Number,
        
    },
    
}, {
    timestamps: true
});

var Weapons = mongoose.model('Weapon', weaponSchema);

module.exports = Weapons;